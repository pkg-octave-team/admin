Administrative support for the Debian Octave Group
==================================================

This repository contains scripts for helping in the maintainance of
packages in the realm of the Debian Octave Group (DOG):

- [dep14-migration](script/dep14-migration): This script was used to
  migrate the Git repositories of the packages maintained by the DOG to the
  new layout proposed in the
  [https://dep-team.pages.debian.net/deps/dep14/](DEP-14). Essentially, the
  branches `master` and `upstream` are renamed to `debian/latest` and
  `upstream/latest`, respectively. This is a one-shot script, intended to
  be used a single time and it may be of interest to other teams in Debian.

- [dep14-update-local-repo](script/dep14-update-local-repo): This is a
  helper script that may be used to update a local cloned repository that
  stil have the `master` and `upstream` branches and whose remote origin
  repository has already transitioned to the new DEP-14 layout.

- [upload-package-avatar](script/upload-package-avatar): This script sets
  the avatar of the Git repository for the Debian packages octave-<name>
  with the image file used at
  [octave.sourceforge.io](https://octave.sourceforge.io) for the
  Octave-Forge package <name>. This is automated via the GitLab API at
  [salsa.debian.org](https://salsa.debian.org).

